from diagrams import Cluster, Diagram, Edge
from diagrams.elastic.elasticsearch import Elasticsearch, Kibana, Monitoring
from diagrams.onprem.container import Docker
from diagrams.custom import Custom

graph_attr = {
    "bgcolor": "transparent"
}

with Diagram("Monitoreo de Contenedores", show=False, filename='monitoreo_contenedores'):

    kibana = Kibana('Kibana')
    logs = Docker('Log de Contenedores')
    metricas = Docker('Metricas de Contenedores')

    with Cluster("Elasticsearch Cluster"):
        es01 = Elasticsearch('nodo 01')
        es02 = Elasticsearch('nodo 02')
        es03 = Elasticsearch('nodo 03')

        es_group = [es01,
                    es02,
                    es03]

    with Cluster('Beats'):
        filebeat = Custom("Filebeat", "./images/beats2.png")
        metricbeat = Custom("Metricbeat", "./images/beats2.png")

    logs << Edge(label='recolecta', color='blue') << filebeat
    metricas << Edge(label='recolecta', color='blue') << metricbeat

    filebeat >> Edge(color='firebrick') >> es_group
    filebeat >> Edge(color='firebrick') >> kibana

    metricbeat >> Edge(color='green') >> es_group
    metricbeat >> Edge(color='green') >> kibana

    es_group >> kibana

