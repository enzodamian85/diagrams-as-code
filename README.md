# Diagrams As Code

Diagramas como codigo  
fuente: https://diagrams.mingrammer.com/  
docker image: samuelsantos/generate-diagrams-as-code

## Uso

1. Clonar el proyecto: git clone https://gitlab.com/enzodamian85/diagrams-as-code
1. Crear archivo {mi-diagrama}.py con el codigo del diagrama en la raiz
1. Ejecutar:
```bash 
docker-compose run -e DIAGRAM_FILE={mi-diagrama}.py diagramas
```

Ejemplo incluido en este repositorio:
``` 
docker-compose run -e DIAGRAM_FILE=diagram.py diagramas
``` 