from diagrams import Cluster, Diagram, Edge
from diagrams.onprem.vcs import Gitlab
#from diagrams.onprem.database import Oracle
from diagrams.programming.framework import Laravel
from diagrams.programming.framework import React
from diagrams.gcp.storage import GCS
from diagrams.custom import Custom

graph_attr = {
    "bgcolor": "transparent"
}

with Diagram("Deployment Caja Chica en Ambiente de Desarrollo y Testing", show=False, filename='deploy-caja-chica'):

    with Cluster('Ambiente Producción'):
        login = Custom("login.psa.gob.ar", "./images/psa_escudo.png")
        gitlab = Gitlab('gitlab.psa.gob.ar')

    with Cluster("Ambiente Desarrollo"):
        with Cluster("api-caja-chica-desa.psa.gob.ar"):
            api_desa = Laravel('PHP 7.2')
            json_desa = Custom('/db-dir', './images/json.png')
            storage_desa = GCS('/imagenes')

            json_desa << Edge(label='curl', color="firebrick", style="dashed") << api_desa
            storage_desa << Edge(label='almacena', color="firebrick", style="dashed") << api_desa

        with Cluster("caja-chica-desa.psa.gob.ar"):
            app_desa = React('Caja Chica')

        with Cluster("Oracle 19c"):
            with Cluster("dadmin.psa.gob.ar"):
                db_desa = Custom('BD CAJA_CHICA', './images/oracle.png')



    with Cluster("Ambiente Testing"):
        with Cluster("api-caja-chica-test.psa.gob.ar"):
            api_test = Laravel('PHP 7.2')
            json_test = Custom('/db-dir', './images/json.png')
            storage_test = GCS('/imagenes')

            storage_test << Edge(label='almacena', color="firebrick", style="dashed") << api_test
            json_test << Edge(label='curl', color="firebrick", style="dashed") << api_test


        with Cluster("caja-chica-test.psa.gob.ar"):
            app_test = React('Caja Chica')

        with Cluster("Oracle 19c"):
            with Cluster("tadmin.psa.gob.ar"):
                db_test = Custom('BD CAJA_CHICA', './images/oracle.png')

    api_desa << app_desa
    api_desa >> db_desa
    gitlab >> api_desa
    gitlab >> app_desa
    login << api_desa
    login << app_desa

    

    api_test << app_test
    api_test >> db_test
    gitlab >> api_test
    gitlab >> app_test
    login << api_test
    login << app_test